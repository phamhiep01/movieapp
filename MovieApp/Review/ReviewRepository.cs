﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly MovieContext _context;
        public ReviewRepository(MovieContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Review>> GetAllReviewsAsync()
        {
            return await _context.Reviews.ToListAsync();
        }
        public async Task<Review> GetReviewByIdAsync(int id)
        {
            return await _context.Reviews.FindAsync(id);
        }

        public async Task AddReviewAsync(Review review)
        {
            await _context.Reviews.AddAsync(review);
            
        }

        public async Task UpdateReviewAsync(Review review)
        {
            _context.Reviews.Update(review);
            
        }

        public async Task DeleteReviewAsync(int id)
        {
            var review = await _context.Reviews.FindAsync(id);
            if (review != null)
            {
                _context.Reviews.Remove(review);
            }
        }

        // Các phương thức cài đặt các thao tác lọc với cơ sở dữ liệu
        public async Task<IEnumerable<Review>> GetReviewsByMovieIdAsync(int movieId)
        {
            return await _context.Reviews.Where(r => r.MovieId == movieId).ToListAsync();
        }

        public async Task<IEnumerable<Review>> GetReviewsByRatingAsync(int rating)
        {
            return await _context.Reviews.Where(r => r.Rating == rating).ToListAsync();
        }

    }
}

