﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class Review
{
    [Key]
    public int Id { get; set; }
    [Required]
    public string Title { get; set; }
    [Required]
    public string Content { get; set; }
    [Required]
    [Range(1, 5)]
    public decimal Rating { get; set; }
    [ForeignKey("Movie")]
    public int MovieId { get; set; }
    public Movie Movie { get; set; }
}





