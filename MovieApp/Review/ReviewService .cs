﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class ReviewService : IReviewService
    {
        public IUnitOfWork UnitOfWork { get; set; }
        public ReviewService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }
        public async Task AddReview (int movieId)
        {
            var movie = UnitOfWork.MovieRepository.GetMovieByIdAsync(movieId);
            if (movie == null) 
            {
                throw new Exception("Không tìm thấy bộ phim với Id này");
            }
            // Nếu tìm thấy, hỏi người dùng nhập vào thông tin của đánh giá mới
            Console.WriteLine("Nhập vào thông tin của đánh giá mới:");

            // Hỏi người dùng nhập vào tiêu đề của đánh giá
            Console.Write("Tiêu đề: ");
            string title = Console.ReadLine();

            // Hỏi người dùng nhập vào nội dung của đánh giá
            Console.Write("Nội dung: ");
            string content = Console.ReadLine();

            // Hỏi người dùng nhập vào xếp hạng của đánh giá
            Console.Write("Xếp hạng (1-5): ");
            int rating = int.Parse(Console.ReadLine());

            // Tạo một đối tượng Review mới với thông tin vừa nhập
            var review = new Review()
            {
                Title = title,
                Content = content,
                Rating = rating,
                MovieId = movieId // Gán khóa ngoại cho MovieId
            };

           await UnitOfWork.ReviewRepository.AddReviewAsync(review);
           await UnitOfWork.SaveChangesAsync();
        }
        public async Task<IEnumerable<Review>> ViewReviews(int movieId)

        {

            Console.WriteLine($"Here are the reviews of movie {movieId}:");

            // Find the movie by Id from the database
            var movie = await UnitOfWork.MovieRepository.GetMovieByIdAsync(movieId);

            // Check if the movie exists
            if (movie is not null)
            {
                // Get the reviews of the movie from the database
                var reviews =await UnitOfWork.ReviewRepository.GetReviewsByMovieIdAsync(movieId);

                // Check if any reviews are found
                if (reviews is not null )
                {
                    // Display the reviews
                    foreach (var review in reviews)
                    {
                        Console.WriteLine($"Id: {review.Id}");
                        Console.WriteLine($"Title: {review.Title}");
                        Console.WriteLine($"Content: {review.Content}");
                        Console.WriteLine($"Rating: {review.Rating}");
                        Console.WriteLine();
                    }
                }

                else
                {
                    // Movie not found
                    Console.WriteLine($"Movie {movieId} not found.");
                    Console.WriteLine();

                   ;
                }
            }
            return await UnitOfWork.ReviewRepository.GetAllReviewsAsync();
        }
    }
}
