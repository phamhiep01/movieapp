﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class BloggingContextFactory : IDesignTimeDbContextFactory<MovieContext>
    {
        public MovieContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<MovieContext>();
            optionsBuilder.UseSqlServer("Data Source=DESKTOP-9FP7ODG;Database=MovieApp;Trusted_Connection=True;TrustServerCertificate=True");

            return new MovieContext(optionsBuilder.Options);
        }
    }
}
