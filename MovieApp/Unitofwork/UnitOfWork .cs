﻿using Microsoft.EntityFrameworkCore;
using MovieReviewApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MovieContext _context;
        private IReviewRepository _reviewRepository;
        private IMovieRepository _movieRepository;
       
        public UnitOfWork(MovieContext context) 
        {
            _context = context;
        }

       

        public IMovieRepository MovieRepository
        {
            get
            {
                if (_movieRepository is null)
                {
                    _movieRepository = new MovieRepository(_context);
                }
                return _movieRepository;
            }
        }

        public IReviewRepository ReviewRepository
        {
            get
            {
                if (_reviewRepository is null)
                {
                    _reviewRepository = new ReviewRepository(_context);
                }
                return _reviewRepository;
            }
        }
        public async Task SaveChangesAsync() => await _context.SaveChangesAsync();
        public void Dispose() 
        {
            _context.Dispose();
        }
    }

}

