﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
     public interface IMovieRepository
    {
        Task<IEnumerable<Movie>> GetAllMoviesAsync(); 
        Task<Movie> GetMovieByIdAsync(int id); 
        Task AddMovieAsync(Movie movie); 
        Task UpdateMovieAsync(Movie movie); 
        Task DeleteMovieAsync(int id); 
        Task<IEnumerable<Movie>> GetMoviesByTitleAsync(string title); 
        Task<IEnumerable<Movie>> GetMoviesByYearAsync(int year);
        Task<IEnumerable<Movie>> GetMoviesByDirectorAsync(string director); 
        Task<IEnumerable<Movie>> GetMoviesByCountryAsync(string country); 
    }
}
