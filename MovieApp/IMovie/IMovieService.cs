﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public interface IMovieService
    {
        IUnitOfWork UnitOfWork { get; } 
        Task<IEnumerable<Movie>> ViewMovies(); 
        Task<Movie> ViewMovieDetails(int id); 
        Task EditMovie(int movieId); 
        Task DeleteMovie(int id); 
        Task AddMovie(string title, int year, string director, string country); 
        Task<IEnumerable<Movie>> FilterMoviesByTitle(string title); 
        Task<IEnumerable<Movie>> FilterMoviesByYear(int year); 
        Task<IEnumerable<Movie>> FilterMoviesByDirector(string director); 
        Task<IEnumerable<Movie>> FilterMoviesByCountry(string country); 
    }
}

