﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public interface IReviewService
    {
        IUnitOfWork UnitOfWork { get; }
        Task AddReview(int movieId);
        Task <IEnumerable<Review>> ViewReviews(int movieId);
    }
}
