﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public interface IReviewRepository
    {
        Task<IEnumerable<Review>> GetAllReviewsAsync(); 
        Task<Review> GetReviewByIdAsync(int id); 
        Task AddReviewAsync(Review review); 
        Task UpdateReviewAsync(Review review); 
        Task DeleteReviewAsync(int id); 
        Task<IEnumerable<Review>> GetReviewsByMovieIdAsync(int movieId); 
        Task<IEnumerable<Review>> GetReviewsByRatingAsync(int rating); 
    }
}
