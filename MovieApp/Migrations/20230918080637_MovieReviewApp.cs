﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieApp.Migrations
{
    /// <inheritdoc />
    public partial class MovieReviewApp : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Year = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Reviews",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Rating = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reviews_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Country", "Director", "Title", "Year" },
                values: new object[,]
                {
                    { 1, "Portugal", "Arvy Higgoe", "Ms", 2006 },
                    { 2, "Russia", "Alva Woollhead", "Ms", 2014 },
                    { 3, "Indonesia", "Federica Luke", "Mr", 1997 },
                    { 4, "United States", "Far Platts", "Mrs", 2018 },
                    { 5, "Russia", "Robers Chiswell", "Mrs", 2007 },
                    { 6, "China", "Annie MacCrossan", "Honorable", 2000 },
                    { 7, "Sweden", "Franciskus Borzone", "Ms", 2020 },
                    { 8, "Philippines", "Salvidor Parnham", "Dr", 2011 },
                    { 9, "Poland", "Nicky Egdale", "Honorable", 2013 },
                    { 10, "China", "Ceil Clowney", "Ms", 2021 },
                    { 11, "Indonesia", "Micheline Trank", "Ms", 2017 },
                    { 12, "Brazil", "Cherianne Galley", "Mrs", 2001 },
                    { 13, "Sweden", "Berkley Tames", "Honorable", 1999 },
                    { 14, "Brazil", "Geri Borer", "Honorable", 2006 },
                    { 15, "Thailand", "Borden Undrill", "Mrs", 2012 },
                    { 16, "Venezuela", "Amie Safont", "Mr", 2000 },
                    { 17, "Argentina", "Eugenio Greenhalf", "Mr", 2014 },
                    { 18, "Philippines", "Patten Bachs", "Mrs", 1996 },
                    { 19, "Dominican Republic", "Fifi McKinnon", "Rev", 2007 },
                    { 20, "Finland", "Gilberte Couch", "Ms", 2007 },
                    { 21, "Brazil", "Dario Bonevant", "Mrs", 1996 },
                    { 22, "Iraq", "Donnell Rubert", "Dr", 2001 },
                    { 23, "Indonesia", "Kerwinn Bleyman", "Ms", 2004 },
                    { 24, "Philippines", "Inigo Dillinger", "Ms", 2003 },
                    { 25, "China", "Tracie Fallens", "Mrs", 2001 },
                    { 26, "China", "Tiffi Kohrding", "Honorable", 2018 },
                    { 27, "Poland", "Gladi Harmond", "Mrs", 1998 },
                    { 28, "Russia", "Sabina Rosten", "Mr", 2021 },
                    { 29, "China", "Mickie Baal", "Mr", 2009 },
                    { 30, "Portugal", "Madison Korda", "Ms", 2002 },
                    { 31, "China", "Gabriel Bichener", "Honorable", 2021 },
                    { 32, "Indonesia", "Vern Thoday", "Ms", 2016 },
                    { 33, "Thailand", "Ephrem Dedenham", "Mr", 2012 },
                    { 34, "Japan", "Clem Bracci", "Dr", 2022 },
                    { 35, "Canada", "Gunther Blumson", "Ms", 2013 },
                    { 36, "China", "Buddy Villar", "Mrs", 2005 },
                    { 37, "China", "Alex Mylechreest", "Mrs", 2002 },
                    { 38, "Greece", "Isak Baxandall", "Mr", 2011 },
                    { 39, "China", "Sallyann Sattin", "Dr", 1996 },
                    { 40, "Dominican Republic", "Harri Newvill", "Mr", 2012 },
                    { 41, "Philippines", "Hugh Hurworth", "Mr", 2000 },
                    { 42, "China", "Lyndsie Billison", "Mrs", 2006 },
                    { 43, "Palestinian Territory", "Haskel Bousquet", "Mr", 2017 },
                    { 44, "Mongolia", "Cam Lugg", "Rev", 2015 },
                    { 45, "Mayotte", "Griselda Cutajar", "Dr", 2001 },
                    { 46, "France", "Doralin Nance", "Rev", 1995 },
                    { 47, "Israel", "Guthry Hayler", "Rev", 1998 },
                    { 48, "Netherlands", "Gayle Everit", "Rev", 2011 },
                    { 49, "Philippines", "Karita Fathers", "Ms", 2021 },
                    { 50, "Brazil", "Christie Gloyens", "Ms", 2005 }
                });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "Id", "Content", "MovieId", "Rating", "Title" },
                values: new object[,]
                {
                    { 1, "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.", 1, 3.6m, "Mrs" },
                    { 2, "Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.", 2, 3.2m, "Rev" },
                    { 3, "", 3, 2.6m, "Mrs" },
                    { 4, "", 4, 4.6m, "Dr" },
                    { 5, "Sed ante. Vivamus tortor. Duis mattis egestas metus.", 5, 4.8m, "Mrs" },
                    { 6, "", 6, 1.8m, "Honorable" },
                    { 7, "", 7, 3.3m, "Rev" },
                    { 8, "Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.", 8, 3.0m, "Rev" },
                    { 9, "", 9, 3.4m, "Mrs" },
                    { 10, "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.", 10, 3.3m, "Dr" },
                    { 11, "", 11, 3.6m, "Honorable" },
                    { 12, "", 12, 4.5m, "Honorable" },
                    { 13, "", 13, 4.2m, "Ms" },
                    { 14, "", 14, 4.9m, "Mrs" },
                    { 15, "", 15, 1.2m, "Honorable" },
                    { 16, "Phasellus in felis. Donec semper sapien a libero. Nam dui.", 16, 3.6m, "Mrs" },
                    { 17, "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.", 17, 2.2m, "Mrs" },
                    { 18, "", 18, 4.5m, "Mrs" },
                    { 19, "", 19, 3.5m, "Mr" },
                    { 20, "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.", 20, 2.5m, "Mr" },
                    { 21, "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.", 21, 2.2m, "Dr" },
                    { 22, "", 22, 4.0m, "Dr" },
                    { 23, "", 23, 4.7m, "Ms" },
                    { 24, "", 24, 1.4m, "Rev" },
                    { 25, "", 25, 1.6m, "Rev" },
                    { 26, "", 26, 3.5m, "Ms" },
                    { 27, "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.", 27, 1.1m, "Dr" },
                    { 28, "", 28, 5.0m, "Rev" },
                    { 29, "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.", 29, 5.0m, "Ms" },
                    { 30, "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.", 30, 1.0m, "Mrs" },
                    { 31, "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.", 31, 2.0m, "Ms" },
                    { 32, "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.", 32, 2.4m, "Dr" },
                    { 33, "", 33, 4.7m, "Honorable" },
                    { 34, "", 34, 1.1m, "Rev" },
                    { 35, "Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.", 35, 3.1m, "Honorable" },
                    { 36, "", 36, 3.1m, "Mrs" },
                    { 37, "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.", 37, 4.4m, "Honorable" },
                    { 38, "", 38, 2.1m, "Mr" },
                    { 39, "", 39, 4.3m, "Mrs" },
                    { 40, "", 40, 2.7m, "Honorable" },
                    { 41, "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.", 41, 3.4m, "Honorable" },
                    { 42, "", 42, 2.6m, "Mr" },
                    { 43, "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.", 43, 3.1m, "Ms" },
                    { 44, "", 44, 4.1m, "Honorable" },
                    { 45, "", 45, 3.1m, "Ms" },
                    { 46, "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.", 46, 2.5m, "Dr" },
                    { 47, "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.", 47, 3.1m, "Ms" },
                    { 48, "", 48, 3.7m, "Rev" },
                    { 49, "", 49, 3.4m, "Rev" },
                    { 50, "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.", 50, 3.2m, "Mr" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_MovieId",
                table: "Reviews",
                column: "MovieId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reviews");

            migrationBuilder.DropTable(
                name: "Movies");
        }
    }
}
