﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class MovieService : IMovieService
    {
        private readonly IUnitOfWork _unitOfWork; // Unit of work là một dependency cho service

        // Hàm khởi tạo nhận vào unit of work và inject vào service
        public MovieService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // Thuộc tính trả về unit of work của service
        public IUnitOfWork UnitOfWork => _unitOfWork;

        // Các phương thức cài đặt các chức năng của service, sử dụng unit of work để quản lý các repository và lưu các thay đổi
        public async Task<IEnumerable<Movie>> ViewMovies()
        {
            return await _unitOfWork.MovieRepository.GetAllMoviesAsync();
        }

        public async Task<Movie> ViewMovieDetails(int id)
        {
            return await _unitOfWork.MovieRepository.GetMovieByIdAsync(id);
        }
        public async Task EditMovie(int movieId)
        {
            // Lấy bộ phim có Id trùng với tham số movieId từ bảng Movie
            var movie = _unitOfWork.MovieRepository.GetMovieByIdAsync(movieId);

            // Kiểm tra xem có tìm thấy bộ phim không
            if (movie is null)
            {
                // Nếu không tìm thấy, thông báo cho người dùng
                Console.WriteLine("Không tìm thấy bộ phim có Id là {0}.", movieId);
                return;
            }

            // Nếu tìm thấy, hiển thị thông tin của bộ phim
            Console.WriteLine("Thông tin hiện tại của bộ phim:");
            Console.WriteLine($"Id: {movie.Result.Id}, Tiêu đề: {movie.Result.Title}, Năm sản xuất: {movie.Result.Year}, Đạo diễn: {movie.Result.Director}, Quốc gia: {movie.Result.Country}");

            // Hỏi người dùng muốn cập nhật thông tin nào
            Console.WriteLine("Bạn muốn cập nhật thông tin nào?");
            Console.WriteLine("T: Tiêu đề");
            Console.WriteLine("Y: Năm sản xuất");
            Console.WriteLine("D: Đạo diễn");
            Console.WriteLine("C: Quốc gia");
            Console.WriteLine("B: Quay lại menu chính");

            // Lấy lựa chọn của người dùng
            string choice = Console.ReadLine();

            // Xử lý lựa chọn của người dùng
            switch (choice.ToUpper())
            {
                case "T": 
                    Console.Write("Nhập vào tiêu đề mới: ");
                    string newTitle = Console.ReadLine();

                    movie.Result.Title = newTitle;
                    await UnitOfWork.SaveChangesAsync();
                    await UnitOfWork.MovieRepository.UpdateMovieAsync(movie.Result);
                    Console.WriteLine("Đã cập nhật tiêu đề thành công.");
                    break;
                case "Y": 
                    Console.Write("Nhập vào năm sản xuất mới: ");
                    int newYear = int.Parse(Console.ReadLine());

                    movie.Result.Year = newYear;
                    await UnitOfWork.SaveChangesAsync();
                    await UnitOfWork.MovieRepository.UpdateMovieAsync(movie.Result);
                    Console.WriteLine("Đã cập nhật năm sản xuất thành công.");
                    break;
                case "D": 
                    Console.Write("Nhập vào đạo diễn mới: ");
                    string newDirector = Console.ReadLine();

                    movie.Result.Director = newDirector;
                    await UnitOfWork.SaveChangesAsync();
                    await UnitOfWork.MovieRepository.UpdateMovieAsync(movie.Result);
                    Console.WriteLine("Đã cập nhật đạo diễn thành công.");
                    break;
                case "C": 
                    Console.Write("Nhập vào quốc gia mới: ");
                    string newCountry = Console.ReadLine();

                    movie.Result.Country = newCountry;
                    await UnitOfWork.SaveChangesAsync();
                    await UnitOfWork.MovieRepository.UpdateMovieAsync(movie.Result);
                    Console.WriteLine("Đã cập nhật quốc gia thành công.");
                    break;
                case "B": 
                    return;
                default: 
                    Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng nhập lại.");
                    break;
            }
           
        }
        public async Task DeleteMovie(int id) 
        {
            var movie =UnitOfWork.MovieRepository.GetMovieByIdAsync(id);
            if (movie is null) 
            {
                throw new Exception("không tìm thấy bộ phim với id này");
            }
           await UnitOfWork.MovieRepository.DeleteMovieAsync(id);
           await UnitOfWork.SaveChangesAsync();
        }
        public async Task AddMovie(string title, int year, string director, string country)
        {
            var movie = new Movie
            {
                Title = title,
                Year = year,
                Director = director,
                Country = country
            };
            if (movie is null)
            {
                throw new Exception("Điền không hơp");
            }

           await UnitOfWork.MovieRepository.AddMovieAsync(movie);
           await UnitOfWork.SaveChangesAsync();
        }
        public async Task<IEnumerable<Movie>> FilterMoviesByTitle(string title)
        {
            return await _unitOfWork.MovieRepository.GetMoviesByTitleAsync(title);
        }

        public async Task<IEnumerable<Movie>> FilterMoviesByYear(int year)
        {
            return await _unitOfWork.MovieRepository.GetMoviesByYearAsync(year);
        }

        public async Task<IEnumerable<Movie>> FilterMoviesByDirector(string director)
        {
            return await _unitOfWork.MovieRepository.GetMoviesByDirectorAsync(director);
        }

        public async Task<IEnumerable<Movie>> FilterMoviesByCountry(string country)
        {
            return await _unitOfWork.MovieRepository.GetMoviesByCountryAsync(country);
        }


    }
}
