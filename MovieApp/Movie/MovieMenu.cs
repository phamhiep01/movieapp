﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class MovieMenu : IMenu
    {
        private readonly Movie _movie;

        public MovieMenu(Movie movie)
        {
            _movie = movie;
        }
        public async void Display()
        {
            Console.WriteLine($"Bạn đang xem thông tin chi tiết của bộ phim {_movie.Title}"); 
            Console.WriteLine($"Năm sản xuất: {_movie.Year}"); 
            Console.WriteLine($"Đạo diễn: {_movie.Director}"); 
            Console.WriteLine($"Quốc gia: {_movie.Country}");

            Console.WriteLine("Vui lòng chọn một trong các tùy chọn sau:"); 
            Console.WriteLine("1. Chỉnh sửa thông tin"); 
            Console.WriteLine("2. Xóa bộ phim"); 
            Console.WriteLine("3. Thêm đánh giá");
            Console.WriteLine("4. Xem tất cả đánh giá");
            Console.WriteLine("5. Quay lại");
        }
       
    }
}

    

