﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MovieApp;

namespace MovieReviewApp
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieContext _context;
        public MovieRepository(MovieContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        public async Task<Movie> GetMovieByIdAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        public async Task AddMovieAsync(Movie movie)
        {

            await _context.Movies.AddAsync(movie);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Movies.Update(movie);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie != null)
            {
                _context.Movies.Remove(movie);
                await _context.SaveChangesAsync();
            }
        }
        public async Task<IEnumerable<Movie>> GetMoviesByTitleAsync(string title)
        {
            return await _context.Movies.Where(m => m.Title.Contains(title)).ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetMoviesByYearAsync(int year)
        {
            return await _context.Movies.Where(m => m.Year == year).ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetMoviesByDirectorAsync(string director)
        {
            return await _context.Movies.Where(m => m.Director.Contains(director)).ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetMoviesByCountryAsync(string country)
        {
            return await _context.Movies.Where(m => m.Country.Contains(country)).ToListAsync();
        }
    }
}
