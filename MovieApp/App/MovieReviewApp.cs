﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class MenuManager
    {
        private MovieService movieService;
        private ReviewService reviewService;
        private int pageSize = 5;
        private int currentPage = 1;
        

        public MenuManager(MovieService movieService, ReviewService reviewService)
        {
            this.movieService = movieService;
            this.reviewService = reviewService;
           
        }


        public void Run()
        {
            Console.WriteLine("Chào mừng bạn đến với ứng dụng đánh giá phim!");
            Menu();
        }
        public void Menu()
        {
            var mainmenu = new MainMenu();
            string choice = Console.ReadLine();
            mainmenu.Display();
           
            switch (choice)
            {
                case "1":
                    ShowMovieList(); // Hiển thị danh sách các bộ phim
                    break;
                case "2":
                    AddMovie(); // Thêm bộ phim mới
                    break;
                case "3":
                    ShowFilterMenu(); // Hiển thị menu lọc phim
                    break;
                case "4":
                    ExitApp(); // Thoát ứng dụng
                    break;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ. Hãy thử lại.");
                    Menu();
                    break;
            }
        }
        public async void ShowMovieList()
        {
            Console.Clear();
            bool back = false;

            while (!back)
            {
                ShowPagedMovies();
                Console.WriteLine("Press N for next page, P for previous page, B for back to main menu or enter movie Id for details.");
                string input = Console.ReadLine();
                switch (input.ToUpper())
                {
                    case "N":
                        currentPage++;
                        ShowMovieList();
                        break;
                    case "P":
                        currentPage--;
                        ShowMovieList();
                        break;
                    case "B":
                        Menu();
                        break;
                    default:
                        int movieId;
                        if (int.TryParse(input, out movieId))
                        {
                            var showMovieDetails = new MovieList(movieService,reviewService);
                            showMovieDetails.ShowMovieDetails(movieId);
                        }
                        else
                        {
                            Console.WriteLine("Invalid input. Please try again.");
                            ShowMovieList();
                        }
                        break;
                }
            }
        }
        public async void ShowPagedMovies()
        {
            IEnumerable<Movie> currentMovies = await movieService.UnitOfWork.MovieRepository.GetAllMoviesAsync();
            int totalMovies = currentMovies.Count(); // Tổng số bộ phim hiện tại
            int totalPages = (int)Math.Ceiling((double)totalMovies / pageSize); // Tổng số trang

            if (currentPage < 1) currentPage = 1; // Nếu trang hiện tại nhỏ hơn 1, đặt nó bằng 1
            if (currentPage > totalPages) currentPage = totalPages; // Nếu trang hiện tại lớn hơn tổng số trang, đặt nó bằng tổng số trang

            Console.WriteLine($"Page {currentPage} of {totalPages}"); // Hiển thị số trang hiện tại và tổng số trang

            var pagedMovies = currentMovies.Skip((currentPage - 1) * pageSize).Take(pageSize); // Lấy các bộ phim theo trang hiện tại

            foreach (var movie in pagedMovies) // Duyệt qua các bộ phim và hiển thị thông tin cơ bản của chúng
            {
                Console.WriteLine($"Id: {movie.Id}, Title: {movie.Title}, Year: {movie.Year}, Director: {movie.Director}, Country: {movie.Country}");
            }
        }



        public async void AddMovie()
        {
            Console.WriteLine("Enter movie title:");
            string title = Console.ReadLine();
            Console.WriteLine("Enter movie year:");
            int year = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter movie director:");
            string director = Console.ReadLine();
            Console.WriteLine("Enter movie country:");
            string country = Console.ReadLine();

            Movie movie = new Movie
            {
                Title = title,
                Year = year,
                Director = director,
                Country = country
            };

           await movieService.UnitOfWork.MovieRepository.AddMovieAsync(movie);
           await movieService.UnitOfWork.SaveChangesAsync();

            Console.WriteLine("Movie added successfully.");
            Menu();
        }


        public void ShowFilterMenu()
        {
            var menu = new FilterMenu();
            menu.Display();
            

            string input = Console.ReadLine(); 
            switch (input) 
            {
                case "1": 
                    Console.WriteLine("Enter movie title:");
                    string title = Console.ReadLine();
                    movieService.UnitOfWork.MovieRepository.GetMoviesByTitleAsync(title);
                    ShowMovieList(); 
                    break;
                case "2": 
                    Console.WriteLine("Enter movie year:");
                    int year = int.Parse(Console.ReadLine()); 
                    movieService.UnitOfWork.MovieRepository.GetMoviesByYearAsync(year); 
                    ShowMovieList(); 
                    break;
                case "3": 
                    Console.WriteLine("Enter movie director:");
                    string director = Console.ReadLine(); 
                    movieService.UnitOfWork.MovieRepository.GetMoviesByDirectorAsync(director); 
                    ShowMovieList(); 
                    break;
                case "4": 
                    Console.WriteLine("Enter movie country:");
                    string country = Console.ReadLine(); 
                    movieService.UnitOfWork.MovieRepository.GetMoviesByCountryAsync(country); 
                    ShowMovieList(); 
                    break;
                case "5": 
                    Menu(); 
                    break;
                default: // Nếu người dùng nhập số khác, thông báo lỗi và gọi lại phương thức này
                    Console.WriteLine("Invalid input. Please try again.");
                    ShowFilterMenu();
                    break;
            }
        }

        public void ExitApp()
        {
            Console.WriteLine("Thank you for using the Movie Review App. Goodbye!");
            Environment.Exit(0); // Kết thúc ứng dụng console
        }
       
        

    }

}



