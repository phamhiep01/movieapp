﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class MovieList
    {
        private MovieService movieService;
        private ReviewService reviewService;
     



        public MovieList(MovieService movieService, ReviewService reviewService)
        {
            this.movieService = movieService;
            this.reviewService = reviewService;
          
        }


        public async void ShowMovieDetails(int movieId)
        {
            var movieid = await movieService.UnitOfWork.MovieRepository.GetMovieByIdAsync(movieId);
            
            if (movieid is null)
            {
                Console.WriteLine("Movie not found. Please try again.");

            }
            else // Nếu tìm thấy bộ phim, hiển thị thông tin chi tiết của nó
            {
                var menuMovie = new MovieMenu(movieid);
                menuMovie.Display();

                string input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        
                       await movieService.EditMovie(movieId);
                        break;
                    case "2":
                        Console.WriteLine("Xóa bộ phim");
                       await movieService.DeleteMovie(movieid.Id);
                        break;
                    case "3":
                        Console.WriteLine("Thêm đánh giá");
                        await reviewService.AddReview(movieId);
                        break;
                    case "4": // Nếu người dùng nhập 4, xem tất cả các đánh giá của bộ phim
                        await reviewService.ViewReviews(movieId); // Gọi phương thức ViewReviews với đối tượng Movie cho trước
                        break;
                    case "5": // Nếu người dùng nhập 5, quay lại danh sách phim
                        var menuManager = new MenuManager(movieService, reviewService);
                        menuManager.ShowMovieList();
                        break;
                    default: // Nếu người dùng nhập số khác, thông báo lỗi và gọi lại phương thức này
                        Console.WriteLine("Invalid input. Please try again.");
                        ShowMovieDetails(movieId);
                        break;
                }
            }
        }
        
      
    }
}
