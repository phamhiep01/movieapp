﻿
using Microsoft.EntityFrameworkCore;

using System.Text.Json;




public class MovieContext : DbContext
{
   
    public DbSet<Movie> Movies { get; set; }
    public DbSet<Review> Reviews { get; set; }
   
    public MovieContext(DbContextOptions<MovieContext> options) : base(options)
    {

    }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Movie>().HasKey(x => x.Id);
        modelBuilder.Entity<Review>().HasKey(x => x.Id);
        modelBuilder.Entity<Movie>()
            .HasMany(m => m.Reviews)
            .WithOne(r => r.Movie)
            .HasForeignKey(r => r.MovieId)
            .OnDelete(DeleteBehavior.Cascade);
        string jsonmovie = File.ReadAllText("MOCK_movie.json");
        string jsonreview = File.ReadAllText("MOCK_review.json");
        List<Movie> movies = JsonSerializer.Deserialize<List<Movie>>(jsonmovie);
        List<Review> reviews = JsonSerializer.Deserialize<List<Review>>(jsonreview);
        modelBuilder.Entity<Movie>().HasData(movies);
        modelBuilder.Entity<Review>().HasData(reviews);
    }

   
}






