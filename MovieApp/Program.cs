﻿
using Microsoft.EntityFrameworkCore;
using MovieApp;
using System;
using System.Text;


try
{
    var factory = new BloggingContextFactory();
    var stringA = new string[1];
    var context = factory.CreateDbContext(stringA);
    var unitofword = new UnitOfWork(context);
    var movieService = new MovieService(unitofword);
    var reviewService = new ReviewService(unitofword);
    Console.OutputEncoding = Encoding.Unicode;
    MenuManager app = new MenuManager(movieService, reviewService);
    app.Run(); // Chạy ứng dụng
    
}
catch (Exception ex)
{

    Console.WriteLine(ex.Message);
}




