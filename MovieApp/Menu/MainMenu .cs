﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class MainMenu : IMenu
    {
        public void Display()
        { 
            Console.WriteLine("Chào mừng bạn đến với ứng dụng đánh giá phim!"); 
            Console.WriteLine("Vui lòng chọn một trong các tùy chọn sau:"); 
            Console.WriteLine("1. Xem danh sách các bộ phim"); 
            Console.WriteLine("2. Thêm bộ phim mới"); 
            Console.WriteLine("3. Lọc phim theo"); 
            Console.WriteLine("4. Thoát ứng dụng"); 
        }
       
    }
}

