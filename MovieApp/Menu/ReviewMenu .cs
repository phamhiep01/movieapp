﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp
{
    public class ReviewMenu :IMenu
    {
        private readonly Review _review;

        public ReviewMenu(Review review) 
        {
            _review = review;
        }

        public void Display() 
        {
            Console.WriteLine($"Bạn đang xem thông tin chi tiết của đánh giá {_review.Title}"); 
            Console.WriteLine($"Nội dung: {_review.Content}"); 
            Console.WriteLine($"Điểm: {_review.Rating}/5"); 

            Console.WriteLine("Vui lòng chọn một trong các tùy chọn sau:"); 
            Console.WriteLine("1. Chỉnh sửa thông tin"); 
            Console.WriteLine("2. Xóa đánh giá"); 
            Console.WriteLine("3. Quay lại"); 
        }

        
    }

}

